<?php
/**
 * Function for new fields
 */
function _ott_fieldscustomtype_installed_fields() {
	$t = get_t();
	return array(
		'ottfieldscustom_category' => array(
			'field_name' => 'ottfieldscustom_category',
			'label' => $t('News Category'),
			'type' => 'text',
			'cardinality' => 3,
		)
	);
}
/**
* Custom Fields for Broadcast News
*/
function add_custom_fields() {
	foreach (_ott_fieldscustomtype_installed_fields() as $field) {
		field_create_field($field);
	}
	foreach (_ott_fieldscustomtype_installed_instances() as $fieldinstance) {
		$fieldinstance['entity_type'] = 'node';
		$fieldinstance['bundle'] = 'ottfields';
		field_create_instance($fieldinstance);
	}
}
/**
* Delete Custom Fields for Broadcast News
*/
function delete_custom_fields() {
	foreach (array_keys(_ott_fieldscustomtype_installed_fields()) as $field) {
		field_delete_field($field);
	}
	$instances = field_info_instances('node', 'ottfields');
	foreach ($instances as $instance_name => $fieldinstance) {
		field_delete_instance($fieldinstance);
	}
}