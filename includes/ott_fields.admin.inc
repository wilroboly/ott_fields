<?php
function ott_fields_config() {
  $instances = field_info_instances();
  $field_types = field_info_field_types();
  $bundles = field_info_bundles();
  $modules = system_rebuild_module_data();
  $header = array(t('Field name'), t('Field type'), t('Used in'), t('Action'));
  $rows = array();
  foreach ($instances as $entity_type => $type_bundles) {
    foreach ($type_bundles as $bundle => $bundle_instances) {
      foreach ($bundle_instances as $field_name => $instance) {
        $field = field_info_field($field_name);
        // Initialize the row if we encounter the field for the first time.
        if (!isset($rows[$field_name])) {
          $rows[$field_name]['class'] = $field['locked'] ? array('menu-disabled') : array('');
          $rows[$field_name]['data'][0] = $field['locked'] ? t('@field_name (Locked)', array('@field_name' => $field_name)) : $field_name;
          $module_name = $field_types[$field['type']]['module'];
          $rows[$field_name]['data'][1] = $field_types[$field['type']]['label'] . ' ' . t('(module: !module)', array('!module' => $modules[$module_name]->info['name']));
        }
        // Add the current instance.
				$admin_path = _field_ui_bundle_admin_path($entity_type, $bundle);
				if (isset($bundles[$entity_type][$bundle])) {
					$rows[$field_name]['data'][2][] = $admin_path ? l($bundles[$entity_type][$bundle]['label'], $admin_path . '/fields') : $bundles[$entity_type][$bundle]['label'];
				} else {
					$rows[$field_name]['data'][2][] = '';
				}
			}
    }
  }
  foreach ($rows as $field_name => $cell) {
		$rows[$field_name]['data'][2] = '';
		foreach ($cell['data'][2] as $key => $value) {
			preg_match('/^<a.*?href=(["\'])(.*?)\1.*$/', $value, $m);
			$link = ltrim($m[2], '/');
			$manage = l(t('Manage Fields'), $link);
			if (isset($value) && $value > '') {
				$rows[$field_name]['data'][2] .= $value . ' - ' . $manage . ' <br />';
			}
			$rows[$field_name]['data'][3] = l(t('Add to Content-type'), 'admin/config/ott_fields/settings/content-type/add/' . $field_name);
		}
	}
  if (empty($rows)) {
    $output = t('No fields have been defined yet.');
  } else {
    // Sort rows by field name.
    ksort($rows);
    $output = theme('table', array('header' => $header, 'rows' => $rows));
  }
  return $output;
}
function ott_fields_contenttype_add($f, $obj, $field) {
	$fieldexists = db_select('field_config_instance', 'fci')
			->fields('fci', array('data'))
			->condition('deleted', 0)
			->condition('field_name', $field)
			->execute()
			->fetchField();
	$uns = unserialize($fieldexists);
	$options = array();
	$options[''] = t('Select One');
	foreach (node_type_get_types() as $key => $val) {
		$fieldexists = db_select('field_config_instance', 'fci')
			->fields('fci', array('bundle', 'field_name'))
			->condition('deleted', 0)
			->condition('field_name', $field)
			->condition('bundle', $key)
			->execute()
			->fetchField();
		if (!isset($fieldexists) || empty($fieldexists) || $fieldexists == '') {
			$options[$key] = $val->name;
		}
	}
	if (empty($options) || (count($options) == 1 || count($options) == 0)) {
		$form['ott_fields_field_name'] = array(
			'#type' => 'hidden',
			'#value' => $field,
			'#weight' => 2
		);
		drupal_set_message(t('This field is already part of all Content-Types.'), 'warning');
		return $form;
	} else {
		$form['ott_fields_content_types'] = array(
			'#type' => 'select',
			'#title' => t('Add the field <em>' . $field . '</em> to a Content-Type'),
			'#options' => $options,
			'#default_value' => '',
			'#required' => TRUE, 
			'#weight' => 1
		);
		$form['ott_fields_field_name'] = array(
			'#type' => 'hidden',
			'#value' => $field,
			'#weight' => 2
		);
		$form['ott_fields_field_label'] = array(
			'#type' => 'hidden',
			'#value' => $uns['label'],
			'#weight' => 3
		);
		$form['ott_fields_field_label_desc'] = array(
			'#type' => 'hidden',
			'#value' => $uns['label'],
			'#weight' => 4
		);
		$form['ott_fields_submit'] = array(
			'#type' => 'submit',
			'#value' => t('Add to Content-Type'),
			'#weight' => 5
		);
		return $form;
	}
}
function ott_fields_contenttype_add_validate($form, &$form_state) {
	if (!isset($form_state['values']) || $form_state['values']['ott_fields_content_types'] == '' || !isset($form_state['values']['ott_fields_content_types'])){
    form_set_error('ott_fields_content_types', t('Please select a Content-Type from the drop-down menu.'));
  }
}
function ott_fields_contenttype_add_submit($form, &$form_state) {
	$instance = array(
		'field_name' => $form['ott_fields_field_name']['#value'],
		'entity_type' => 'node',
		'bundle' => $form['ott_fields_content_types']['#value'],
		'label' => $form['ott_fields_field_label']['#value'],
		'description' => $form['ott_fields_field_label_desc']['#value'],
		'required' => TRUE,
	);
	field_create_instance($instance);
	drupal_goto('admin/config/ott_fields/settings');
	drupal_set_message(t('@fieldname Field has been added to the Content-Type @contenttype'), array('@fieldname' => $form['ott_fields_field_name']['#value'], '@contenttype', $form['ott_fields_content_types']['#value']), 'status');
}
function ott_fields_contenttype_add_title($field) {
	return t('Add Field !field to a Content-Type', array('!field' => $field));
}
function ott_field_delete_field_form($form, &$form_state, $instance) {
  module_load_include('inc', 'field_ui', 'field_ui.admin');
  $output = field_ui_field_delete_form($form, $form_state, $instance);
  $output['actions']['cancel'] = array(
		'#markup' => l(t('Cancel'), 'admin/config/group/fields'),
  );
  return $output;
}