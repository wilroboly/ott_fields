<?php
/**
 * Function for instances
 */
function _ott_fieldscustomtype_installed_instances() {
	$t = get_t();
	return array(
		'ottfieldscustom_category' => array(
			'field_name' => 'ottfieldscustom_category',
			'label' => $t('News Category'),
			'bundle' => 'ottfields',
			'entity_type' => 'node', 
			'required' => TRUE,
			'widget' => array(
				'type' => 'text'
			),
			'display' => array(
				'default' => array(
					'label' => 'hidden',
					'type' => 'text_plain'
				)
			),
			'description' => 'News Categories',
		)
	);
}